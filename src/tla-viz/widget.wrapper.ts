import { Type } from '@angular/core';
export class WidgetWrapper {
    public data: string;
    constructor(public component: Type<any>, public name: string) {}
}