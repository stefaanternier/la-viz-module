
import { Component, Input, AfterViewInit, ViewChild, ComponentFactoryResolver, OnDestroy, Directive, ViewContainerRef } from '@angular/core';
import { VizDirective } from './visualisation.directive';
import { DynamicComponentsService } from './dynamic.components.service'
import { WidgetWrapper } from './widget.wrapper';
import { ParentComponent } from './widgets/parent.component';

@Component({
    selector: 'viz-widget',
    template: `
        <div class="box">
            <div class="box-header with-border">
                <h3 class="box-title">{{widgetTitle}}</h3>

                <div class="box-body">
                
                <ng-template viz-host></ng-template>
                </div>
            </div>
        </div>
            `
})
export class VisualisationWidget implements AfterViewInit, OnDestroy {
    @Input() id: string;
    @Input() data: any;
    @Input() widgetTitle: string;
    @ViewChild(VizDirective) vizHost: VizDirective;

    wrapper: WidgetWrapper;

    constructor(
        private _componentFactoryResolver: ComponentFactoryResolver,
        private dynamicComponentsService: DynamicComponentsService) {
    }

    ngAfterViewInit() {
        setTimeout(() => {
            this.loadComponent();

        }, 100);
    }
    ngOnDestroy() {
    }
    loadComponent() {
        this.wrapper = this.dynamicComponentsService.getVisualisations()[this.id];
        let componentFactory = this._componentFactoryResolver.resolveComponentFactory(this.wrapper.component);
        let viewContainerRef = this.vizHost.viewContainerRef;
        viewContainerRef.clear();
        let componentRef = viewContainerRef.createComponent(componentFactory);
        (<ParentComponent>componentRef.instance).setData(this.data);

    }
}
