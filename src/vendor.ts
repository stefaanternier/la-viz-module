// Angular
import '@angular/platform-browser';
import '@angular/platform-browser-dynamic';
import '@angular/core';
import '@angular/common';
import '@angular/http';
import '@angular/router';

import 'jquery';
import 'bootstrap-loader';

// RxJS
import 'rxjs';

// Other vendors for example jQuery, Lodash or Bootstrap
// You can import js, ts, css, sass, ...

import './dist/js/adminlte.min.js';


import './dist/bootstrap/dist/css/bootstrap.min.css';
import './dist/font-awesome/css/font-awesome.min.css';
import './dist/css/AdminLTE.min.css';
import './dist/css/_all-skins.min.css';