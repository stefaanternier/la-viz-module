import { Component, Input }  from '@angular/core';
import { ParentComponent }       from '../parent.component';


@Component({
    providers: [],    
  template: `
    <div >
    Here comes an array
      <div class="row">
      <div class="col-sm-12">
        <table id="example1" class="table table-bordered table-striped dataTable" role="grid" aria-describedby="example1_info">
          <thead>
            <tr role="row" >

              <th class="sorting_asc" *ngFor="let col of result.cols" tabindex="0" aria-controls="example1" rowspan="1" colspan="1" aria-sort="ascending"
                aria-label="Rendering engine: activate to sort column descending">{{col.label}}</th>

          </thead>
          <tbody>
            <tr role="row" class="odd" *ngFor="let row of result.rows">
              <td *ngFor="let cell of row.c">{{cell.v}}</td>

            </tr>

          </tbody>

        </table>
      </div>
    </div>
    </div>
  `
})
export class ArrayVizComponent implements ParentComponent {
  @Input() data: any;
  
  result :any;

  constructor() { 
    this.result = {
      cols: [],
      rows: []
    } 
  }
//
setData(data:any){
   // this.data = data;
}
  
}