import { Component, OnInit, Input } from '@angular/core';

@Component({
    selector: 'hw-viz-widget',
    template: `<p>This is a visualisation component, that displays incomming data is a json tree <br> {{data|json}}</p>`
})
export class HwVisualisationWidget implements OnInit {
    @Input('data') data:any;


    constructor() { }

    ngOnInit() {
    }

}