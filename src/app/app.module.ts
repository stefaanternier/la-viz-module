import { NgModule } from '@angular/core';
import { BrowserModule } from '@angular/platform-browser';

import { AppComponent } from './app.component';
import { DefaultComponent } from './default.component';
import { RouterModule } from '@angular/router';
import { HttpModule }    from '@angular/http';

import { VisualisationModule } from '../tla-viz/tla-viz.module';



@NgModule({
  imports: [
    BrowserModule,
    HttpModule,
    VisualisationModule.forRoot(),
    RouterModule.forRoot([
      {
        path: '',
        redirectTo: '/defaultVisualisation',
        pathMatch: 'full'
      }, {
        path: 'defaultVisualisation',
        component: DefaultComponent
      }
    ])
  ],
  
  
  declarations: [
  
    AppComponent,
    DefaultComponent
  ],
  bootstrap: [AppComponent] 
})
export class AppModule { }
