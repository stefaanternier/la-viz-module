import { Injectable }           from '@angular/core';

import { ArrayVizComponent }   from './widgets/custom/array.viz.component';
import { TextVizComponent }   from './widgets/custom/text.viz.component';
// import { TextVizComponent }   from './text.viz.component';
import { BarchartVizComponent }   from './widgets/ngchart/barchart.viz.component';
// import { LinechartVizComponent }   from './linechart.viz.component';
// import { RadarchartVizComponent }   from './radarchart.viz.component';
import { WidgetWrapper }               from './widget.wrapper';

@Injectable()
export class DynamicComponentsService {
 
 getVisualisations() {
   return {
      array: new WidgetWrapper(ArrayVizComponent, "Array Widget"),
      text: new WidgetWrapper(TextVizComponent, "Array Widget"),
      barchart : new WidgetWrapper(BarchartVizComponent, "Array Widget")
    //  text: new VizItem(TextVizComponent,   {headline: 'Hiring for several positions'}),
    //  barchart: new VizItem(BarchartVizComponent,   {headline: 'Hiring for several positions'}),
    //  linechart: new VizItem(LinechartVizComponent,   {headline: 'Hiring for several positions'}),
    //  radarchart: new VizItem(RadarchartVizComponent,   {headline: 'Hiring for several positions'})
   };
 }
}