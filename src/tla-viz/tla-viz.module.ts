import { NgModule, ModuleWithProviders } from '@angular/core';
import { CommonModule } from '@angular/common';

import { HwVisualisationWidget } from './helloworld.visualisation.widget';
import { VisualisationWidget } from './visualisation.widget';

import {DynamicComponentsService} from './dynamic.components.service';
import { VizDirective } from './visualisation.directive';

import { ChartsModule } from 'ng2-charts';



import {ArrayVizComponent} from './widgets/custom/array.viz.component'
import {TextVizComponent} from './widgets/custom/text.viz.component'
import { BarchartVizComponent }   from './widgets/ngchart/barchart.viz.component';

@NgModule({
    imports: [CommonModule,ChartsModule],
    declarations: [
        HwVisualisationWidget,
        VisualisationWidget,
        VizDirective,
        ArrayVizComponent,
        TextVizComponent,
        BarchartVizComponent
        
        
    ],
    entryComponents: [
        ArrayVizComponent,
        TextVizComponent,
        BarchartVizComponent
      ],
    providers: [DynamicComponentsService],
    exports: [HwVisualisationWidget, VisualisationWidget]
})
export class VisualisationModule {
    static forRoot(): ModuleWithProviders {
        return {
            ngModule: VisualisationModule
        }
    }
}