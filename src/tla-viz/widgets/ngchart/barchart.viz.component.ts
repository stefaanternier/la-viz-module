import { Component, Input } from '@angular/core';
import { ParentComponent }       from '../parent.component';

@Component({
    template: require('./barchart.viz.component.html')
    
})
export class BarchartVizComponent implements ParentComponent {
    @Input() data: any;
    public queryId: string;
    public barChartLabels:string[];
    public barChartType: string;
    public barChartLegend:boolean;
    public barChartData:any[];
    public lineChartData:Array<any> = [
        
      ];

    constructor(){
        this.barChartLabels = [];//'2006', '2007', '2008', '2009', '2010', '2011', '2012'
        this.barChartType = 'bar';
        this.barChartLegend= true;
        this.barChartData= [
            { data:[], label: 'Series A' } //65, 59, 80, 81, 56, 55, 40
        ];
        var barChartData:any= [
            { data:[], label: 'Series A' } //65, 59, 80, 81, 56, 55, 40
        ];
    }
    setData(data:any){
        console.log(data);
        this.lineChartData = data;
        // this.data = data;
     }

    show = false;
    
    public barChartOptions: any = {
        scaleShowVerticalLines: false,
        responsive: true
    };

    
    // events
    public chartClicked(e: any): void {
        console.log(e);
    }
}