import {Directive, ViewContainerRef } from '@angular/core';

@Directive({
    selector: '[viz-host]',
})
export class VizDirective {
    constructor(public viewContainerRef: ViewContainerRef) { }
}
